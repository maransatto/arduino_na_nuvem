
#include "DHT.h"
#include "Adafruit_Sensor.h"

#define DHTPIN 2     // what pin we're connected to

#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  //Serial.println("DHT Hidro test!");

  dht.begin();
    
}

void loop() {
  delay(2000);

  float h = dht.readHumidity();

  float t = dht.readTemperature();

  //float f = dht.readTemperature(true);

 
  if (isnan(h) || isnan(t)/* || isnan(f)*/) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

 
  //float hif = dht.computeHeatIndex(f, h);
  //float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(h);
  Serial.print("|");
  Serial.println(t);
 
  }








